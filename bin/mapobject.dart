class Mapobject {
  int x=0;
  int y=0;
  String symbol="";

  mapobject(String symbol, int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  getX() {
    return x;
  }

  getY() {
    return y;
  }

  getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  
}
