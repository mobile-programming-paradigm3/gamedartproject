import 'mapobject.dart';

class Star extends Mapobject {
  int sum = 0;

  Star(int x, int y, int sum) {
    this.x = x;
    this.y = y;
    this.sum = sum;
    this.symbol = "  ⭐  ";
  }

  int addstar() {
    int sumStar = sum;
    symbol = '  —  ';
    sum = 0;
    return sumStar;
  }
}
