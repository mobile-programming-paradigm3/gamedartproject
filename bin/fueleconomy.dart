import 'TableMap.dart';
import 'car.dart';
import 'move.dart';

class FuelEconomy extends Car implements move {
  FuelEconomy(symbol, int x, int y, Tablemap? map, double fuel)
      : super(symbol, x, y, map, fuel) {
    this.symbol = symbol;
    this.x = x;
    this.y = y;
    this.map = map;
    this.fuel = 6;
  }

  @override
  bool move1(String direction) {
    switch (direction) {
      case "w":
        if (walkN()) return false;
        break;
      case "s":
        if (walkS()) return false;
        break;
      case "d":
        if (walkE()) return false;
        break;
      case "a":
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    checkBomb();
    return true;
  }

  bool walkN() {
    checkFuel();
    if (canMove(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    reduceFuel1();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkS() {
    checkFuel();
    if (canMove(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    reduceFuel1();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkE() {
    checkFuel();
    if (canMove(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    reduceFuel1();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkW() {
    checkFuel();
    if (canMove(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    reduceFuel1();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }
}
