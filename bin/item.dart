import 'mapobject.dart';

class Item extends Mapobject{
  Item(int x, int y, int item){
    this.x = x;
    this.y = y;
    this.symbol = "  ❓  ";
  }
}

class ProtectiveShield extends Item{
  int countProtection = 0;
  ProtectiveShield(int x, int y, int countProtection) : super(x, y,countProtection){
    this.countProtection = countProtection;
  }
  
  int addprotection() {
    int count = countProtection;
    symbol = '  —  ';
    countProtection = 0;
    return count;
  }
}

class Attatk extends Item{
  int countAttatk =0;
  Attatk(int x, int y, int countAttatk) : super(x, y, countAttatk){
    this.countAttatk = countAttatk;
  }
  int addAttatk() {
    int count = countAttatk;
    symbol = '  —  ';
    countAttatk = 0;
    return count;
  }

  
}