import 'dart:ffi';
import 'dart:io';

import 'bomb.dart';
import 'car.dart';
import 'fuel.dart';
import 'fuel_very.dart';
import 'fueleconomy.dart';
import 'item.dart';
import 'mapobject.dart';
import 'motorcycle.dart';
import 'obstruction.dart';
import 'star.dart';
import 'tablemap.dart';

class Tablemap {
  var width;
  var heigth;
  FuelEconomy? car;
  Motorcycle? car1;
  FuelVery? car2;
  Bomb? bomb;
  Obstruction? obstruction;
  Mapobject? map;
  List<Mapobject> objects = [];
  int objCount = 0;

  Tablemap(int width, int height) {
    this.width = width;
    this.heigth = height;
  }

  addObj(Mapobject obj) {
    objects.add(obj);
    objCount++;
  }

  void setCar(FuelEconomy car) {
    this.car = car;
    addObj(car);
  }

  void setCar1(Motorcycle car1) {
    this.car1 = car1;
    addObj(car1);
  }
  void setCar2(FuelVery car2) {
    this.car2 = car2;
    addObj(car2);
  }

  void setBomb(Bomb bomb) {
    this.bomb = bomb;
    addObj(bomb);
  }

  void printSymbolOn(int x, int y) {
    var symbol = '  —  ';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        symbol = objects[o].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    print(car!.printStatus());
    for (int y = 0; y < heigth; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      print("");
    }
  }
  void showMap1() {
    print(car1!.printStatus());
    for (int y = 0; y < heigth; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      print("");
    }
  }
  void showMap2() {
    print(car2!.printStatus());
    for (int y = 0; y < heigth; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      print("");
    }
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < heigth);
  }

  bool isBomb(int x, int y) {
    return bomb!.isOn(x, y);
  }

  bool isObstruction(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Obstruction && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  int fillFuel(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Fuel && objects[o].isOn(x, y)) {
        Fuel fuel = objects[o] as Fuel;
        return fuel.fillFuel();
      }
    }
    return 0;
  }

  int addStar(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Star && objects[o].isOn(x, y)) {
        Star star = objects[o] as Star;
        return star.addstar();
      }
    }
    return 0;
  }

  int addItem(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is ProtectiveShield && objects[o].isOn(x, y)) {
        ProtectiveShield protection = objects[o] as ProtectiveShield;
        return protection.addprotection();
      }
    }
    return 0;
  }

  int addItem2(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Attatk && objects[o].isOn(x, y)) {
        Attatk attatk = objects[o] as Attatk;
        return attatk.addAttatk();
      }
    }
    return 0;
  }
}
