import 'mapobject.dart';

class Obstruction extends Mapobject {
  Obstruction(int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = "  🚧  ";
  }
}
