import 'TableMap.dart';
import 'car.dart';
import 'move.dart';

class FuelVery extends Car implements move{
  FuelVery(String symbol, int x, int y, Tablemap? map, double fuel) : super(symbol, x, y, map, fuel);

  @override
  bool move1(String direction) {
    switch (direction) {
      case "w":
        if (walkN()) return false;
        break;
      case "s":
        if (walkS()) return false;
        break;
      case "d":
        if (walkE()) return false;
        break;
      case "a":
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    checkBomb();
    return true;
  }

}