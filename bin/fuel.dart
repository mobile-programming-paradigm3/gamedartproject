import 'mapobject.dart';

class Fuel extends Mapobject {
  int volumn = 0;

  Fuel(int x, int y, int volumn) {
    this.symbol = "  🛢️  ";
    this.x = x;
    this.y = y;
    this.volumn = volumn;
  }

  int fillFuel() {
    int vol = volumn;
    symbol = '  —  ';
    volumn = 0;
    return vol;
  }
}
