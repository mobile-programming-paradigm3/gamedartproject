import 'dart:io';
import 'dart:math';

import 'TableMap.dart';
import 'bomb.dart';
import 'car.dart';
import 'fuel.dart';
import 'fuel_very.dart';
import 'fueleconomy.dart';
import 'item.dart';
import 'mapobject.dart';
import 'motorcycle.dart';
import 'move.dart';
import 'obstruction.dart';
import 'star.dart';

void main(List<String> arguments) {
  ShowDifficultyLevel();
  int N = 0;
  int positionX_car = 0;
  int positionY_car = 0;
  int positionX_bomb = 0;
  int positionY_bomb = 0;
  int positionX_obstruction = 0;
  int positionY_obstruction = 0;
  int positionX_Fuel = 0;
  int positionY_Fuel = 0;
  int positionX_ProtectiveShield = 0;
  int positionY_ProtectiveShield = 0;
  int positionX_Attatk = 0;
  int positionY_Attatk = 0;
  int positionX_star = 0;
  int positionY_star = 0;
  stdout.write("Please select a difficulty level : ");
  String select = (stdin.readLineSync()!);
  if (select == "1") {
    N = 10;
    positionX_car = Random().nextInt(N);
    positionY_car = Random().nextInt(N);
    print(">>> You have chosen the difficulty level : $select : EASY <<<");
  } else if (select == "2") {
    N = 15;
    positionX_car = Random().nextInt(N);
    positionY_car = Random().nextInt(N);
    print(">>> You have chosen the difficulty level : $select : MEDIUM <<<");
  } else if (select == "3") {
    N = 20;
    positionX_car = Random().nextInt(N);
    positionY_car = Random().nextInt(N);
    print(">>> You have chosen the difficulty level : $select : HARD <<<");
  } else {
    N = 10;
    positionX_car = Random().nextInt(N);
    positionY_car = Random().nextInt(N);
    print(">>> You have chosen the difficulty level : $select : EASY <<<");
  }
  // ShowStartGame();
  Tablemap map = Tablemap(N, N);
  FuelEconomy car = FuelEconomy("  🚗  ", positionX_car, positionY_car, map, 1);
  Motorcycle car1 = Motorcycle("  🛵  ", positionX_car, positionY_car, map, 2);
  FuelVery car2 = FuelVery("  🚚  ", positionX_car, positionY_car, map, 20);
  Bomb bomb = Bomb(positionX_bomb = Random().nextInt(N),
      positionY_bomb = Random().nextInt(N));
  int i = 0; // เอาไว้วนลูป addObstruction inmap
  int j = 0; // เอาไว้วนลูป addFuel in map
  int k = 0; // เอาไว้วนลูป addItem in map
  int s = 0; // addStar in map
  if (select == "1") {
    while (j < 2) {
      positionX_Fuel = Random().nextInt(N);
      positionY_Fuel = Random().nextInt(N);
      map.addObj(Fuel(positionX_Fuel, positionY_Fuel, 5));
      j++;
    }

    while (k < 1) {
      positionX_ProtectiveShield = Random().nextInt(N);
      positionY_ProtectiveShield = Random().nextInt(N);
      map.addObj(ProtectiveShield(
          positionX_ProtectiveShield, positionY_ProtectiveShield, 1));
      positionX_Attatk = Random().nextInt(N);
      positionY_Attatk = Random().nextInt(N);
      map.addObj(Attatk(positionX_Attatk, positionY_Attatk, 1));
      k++;
    }
    while (i < 4) {
      positionX_obstruction = Random().nextInt(N);
      positionY_obstruction = Random().nextInt(N);
      map.addObj(Obstruction(positionX_obstruction, positionY_obstruction));
      i++;
    }
    while (s < 3) {
      positionX_star = Random().nextInt(N);
      positionY_star = Random().nextInt(N);
      map.addObj(Star(positionX_star, positionY_star, 1));
      s++;
    }
  } else if (select == "2") {
    while (j < 3) {
      positionX_Fuel = Random().nextInt(N);
      positionY_Fuel = Random().nextInt(N);
      map.addObj(Fuel(positionX_Fuel, positionY_Fuel, 10));
      j++;
    }

    while (k < 2) {
      positionX_ProtectiveShield = Random().nextInt(N);
      positionY_ProtectiveShield = Random().nextInt(N);
      map.addObj(ProtectiveShield(
          positionX_ProtectiveShield, positionY_ProtectiveShield, 1));
      positionX_Attatk = Random().nextInt(N);
      positionY_Attatk = Random().nextInt(N);
      map.addObj(Attatk(positionX_Attatk, positionY_Attatk, 1));
      k++;
    }

    while (i < 8) {
      positionX_obstruction = Random().nextInt(N);
      positionY_obstruction = Random().nextInt(N);
      map.addObj(Obstruction(positionX_obstruction, positionY_obstruction));
      i++;
    }
    while (s < 3) {
      positionX_star = Random().nextInt(N);
      positionY_star = Random().nextInt(N);
      map.addObj(Star(positionX_star, positionY_star, 1));
      s++;
    }
  } else if (select == "3") {
    while (j < 4) {
      positionX_Fuel = Random().nextInt(N);
      positionY_Fuel = Random().nextInt(N);
      map.addObj(Fuel(positionX_Fuel, positionY_Fuel, 20));
      j++;
    }
    while (k < 4) {
      positionX_ProtectiveShield = Random().nextInt(N);
      positionY_ProtectiveShield = Random().nextInt(N);
      map.addObj(ProtectiveShield(
          positionX_ProtectiveShield, positionY_ProtectiveShield, 1));
      positionX_Attatk = Random().nextInt(N);
      positionY_Attatk = Random().nextInt(N);
      map.addObj(Attatk(positionX_Attatk, positionY_Attatk, 1));
      k++;
    }
    while (i < 16) {
      positionX_obstruction = Random().nextInt(N);
      positionY_obstruction = Random().nextInt(N);
      map.addObj(Obstruction(positionX_obstruction, positionY_obstruction));
      i++;
    }
    while (s < 3) {
      positionX_star = Random().nextInt(N);
      positionY_star = Random().nextInt(N);
      map.addObj(Star(positionX_star, positionY_star, 1));
      s++;
    }
  } else {
    while (j < 3) {
      positionX_Fuel = Random().nextInt(N);
      positionY_Fuel = Random().nextInt(N);
      map.addObj(Fuel(positionX_Fuel, positionY_Fuel, 10));
      j++;
    }
    while (k < 2) {
      positionX_ProtectiveShield = Random().nextInt(N);
      positionY_ProtectiveShield = Random().nextInt(N);
      map.addObj(ProtectiveShield(
          positionX_ProtectiveShield, positionY_ProtectiveShield, 1));
      positionX_Attatk = Random().nextInt(N);
      positionY_Attatk = Random().nextInt(N);
      map.addObj(Attatk(positionX_Attatk, positionY_Attatk, 1));
      k++;
    }
    while (i < 8) {
      positionX_obstruction = Random().nextInt(N);
      positionY_obstruction = Random().nextInt(N);
      map.addObj(Obstruction(positionX_obstruction, positionY_obstruction));
      i++;
    }
    while (s < 3) {
      positionX_star = Random().nextInt(N);
      positionY_star = Random().nextInt(N);
      map.addObj(Star(positionX_star, positionY_star, 1));
      s++;
    }
  }
  ShowSelectCar();
  stdout.write("Please select a car : ");
  String select_car = (stdin.readLineSync()!);
  map.setBomb(bomb);
  bool checkCar = true;
  while (checkCar) {
    if (select_car == "1") {
      print(">>> You have selected a car : 1 : Ferrari <<<");
      HowToPlay();
      map.setCar(car);
      car.setMap(map);
      while (true) {
        map.showMap();
        if (car.x == bomb.x && car.y == bomb.y) {
          if (car.itemprotection > 0) {
            print("You lost your shield by stepping on a bomb.");
            car.reduceitemprotection();
          } else {
            ShowFoundBomb();
            ShowGameOver();
            checkCar = false;
            break;
          }
        }
        if (car.star == 3) {
          ShowWin();
          checkCar = false;
          break;
        }
        if (car.fuel == 0) {
          ShowGameOver();
          print("because your fuel runs out");
          checkCar = false;
          break;
        }
        stdout.write("Please type the direction you want = ");
        String move = (stdin.readLineSync()!);
        if (move == "q") {
          print("Byeee");
          checkCar = false;
          break;
        }
        car.move1(move);
      }
    } else if (select_car == "2") {
      print(">>> You have selected a car : 2 : Lamborghini aventador <<<");
      HowToPlay();
      map.setCar1(car1);
      car1.setMap(map);
      while (true) {
        map.showMap1();
        if (car1.x == bomb.x && car1.y == bomb.y) {
          if (car1.itemprotection > 0) {
            print("You lost your shield by stepping on a bomb.");
            car1.reduceitemprotection();
          } else {
            ShowFoundBomb();
            ShowGameOver();
            checkCar = false;
            break;
          }
        }
        if (car1.star == 3) {
          ShowWin();
          checkCar = false;
          break;
        }
        if (car1.fuel == 0) {
          ShowGameOver();
          print("because your fuel runs out");
          checkCar = false;
          break;
        }
        stdout.write("Please type the direction you want = ");
        String move = (stdin.readLineSync()!);
        if (move == "q") {
          print("Byeee");
          checkCar = false;
          break;
        }
        car1.move1(move);
      }
    } else if (select_car == "3") {
      print(">>> You have selected a car : 2 : Nissan GTR R34  <<<");
      HowToPlay();
      map.setCar2(car2);
      car2.setMap(map);
      while (true) {
        map.showMap2();
        if (car2.x == bomb.x && car2.y == bomb.y) {
          if (car2.itemprotection > 0) {
            print("You lost your shield by stepping on a bomb.");
            car2.reduceitemprotection();
          } else {
            ShowFoundBomb();
            ShowGameOver();
            checkCar = false;
            break;
          }
        }
        if (car2.star == 3) {
          ShowWin();
          checkCar = false;
          break;
        }
        if (car2.fuel == 0) {
          ShowGameOver();
          print("because your fuel runs out");
          checkCar = false;
          break;
        }
        stdout.write("Please type the direction you want = ");
        String move = (stdin.readLineSync()!);
        if (move == "q") {
          print("Byeee");
          checkCar = false;
          break;
        }
        car2.move1(move);
      }
    } else {
      print("!!! Please select the car again !!!");
      ShowSelectCar();
      select_car = (stdin.readLineSync()!);
    }
  }

  // map.setCar(car);
}

void HowToPlay() {
  print("===================================================================================================");
  print("+                                    How to play , Rules of play                                  +");
  print("+                                       w = Move forward                                          +");
  print("+                                       a = Move left                                             +");
  print("+                                       s = Move backward                                         +");
  print("+                                       d = Move right                                            +");
  print("+                      There are obstacles, items, oils, stars and bombs in the map.              +");
  print("+                         Player must collect all 3 stars to win.                                 +"); 
  print("+    But if the player steps on the bomb or runs out of fuel The player will be the loser.        +");
  print("+ In the map, there will be items to collect. It will randomize items between shields or attacks. +");
  print("+                   To exit the game while it is playing, press the q key.                        +");
  print("===================================================================================================");
}

void ShowDifficultyLevel() {
  print("===============================================");
  print("+    Please select a difficulty level :       +");
  print("+          1. : EASY                          +");
  print("+          2. : MEDIUM                        +");
  print("+          3. : HARD                          +");
  print("===============================================");
}

void ShowSelectCar() {
  print("===============================================");
  print("+            Please select a car :            +");
  print("+          1. : Ferrari                       +");
  print("+          2. : Lamborghini aventador         +");
  print("+          3. : Nissan GTR R34                +");
  print("===============================================");
}

void ShowFoundBomb() {
  var dungeonMessage = new Runes('''
 \n
██     ██     ██     ███████  ██████  ██    ██ ███    ██ ██████      ██████   ██████  ███    ███ ██████      ██     ██     ██ 
██     ██     ██     ██      ██    ██ ██    ██ ████   ██ ██   ██     ██   ██ ██    ██ ████  ████ ██   ██     ██     ██     ██ 
██     ██     ██     █████   ██    ██ ██    ██ ██ ██  ██ ██   ██     ██████  ██    ██ ██ ████ ██ ██████      ██     ██     ██ 
                     ██      ██    ██ ██    ██ ██  ██ ██ ██   ██     ██   ██ ██    ██ ██  ██  ██ ██   ██                      
██     ██     ██     ██       ██████   ██████  ██   ████ ██████      ██████   ██████  ██      ██ ██████      ██     ██     ██ 
  ''');
  print(new String.fromCharCodes(dungeonMessage));
}

void ShowWin() {
  var dungeonMessage = new Runes('''
 \n
██╗   ██╗ ██████╗ ██╗   ██╗    ██╗    ██╗██╗███╗   ██╗
╚██╗ ██╔╝██╔═══██╗██║   ██║    ██║    ██║██║████╗  ██║
 ╚████╔╝ ██║   ██║██║   ██║    ██║ █╗ ██║██║██╔██╗ ██║
  ╚██╔╝  ██║   ██║██║   ██║    ██║███╗██║██║██║╚██╗██║
   ██║   ╚██████╔╝╚██████╔╝    ╚███╔███╔╝██║██║ ╚████║
   ╚═╝    ╚═════╝  ╚═════╝      ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝
  ''');
  print(new String.fromCharCodes(dungeonMessage));
}

void ShowGameOver() {
  var dungeonMessage = new Runes('''
 \n
 ██████╗  █████╗ ███╗   ███╗███████╗     ██████╗ ██╗   ██╗███████╗██████╗ 
██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔═══██╗██║   ██║██╔════╝██╔══██╗
██║  ███╗███████║██╔████╔██║█████╗      ██║   ██║██║   ██║█████╗  ██████╔╝
██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██║   ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗
╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ╚██████╔╝ ╚████╔╝ ███████╗██║  ██║
 ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝     ╚═════╝   ╚═══╝  ╚══════╝╚═╝  ╚═╝
  ''');
  print(new String.fromCharCodes(dungeonMessage));
}

void ShowStartGame() {
  var dungeonMessage = new Runes('''
 \n


███████╗████████╗ █████╗ ██████╗ ████████╗     ██████╗  █████╗ ███╗   ███╗███████╗
██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝    ██╔════╝ ██╔══██╗████╗ ████║██╔════╝
███████╗   ██║   ███████║██████╔╝   ██║       ██║  ███╗███████║██╔████╔██║█████╗  
╚════██║   ██║   ██╔══██║██╔══██╗   ██║       ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝  
███████║   ██║   ██║  ██║██║  ██║   ██║       ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗
╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝        ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
                                                                                  
  ''');
  print(new String.fromCharCodes(dungeonMessage));
}
