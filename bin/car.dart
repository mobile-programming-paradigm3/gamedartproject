import 'TableMap.dart';
import 'mapobject.dart';
import 'move.dart';

class Car extends Mapobject implements move{
  Tablemap? map;
  double fuel = 0;
  int star = 0;
  int itemprotection = 0;
  int itemAttatk = 0;
  Car(String symbol, int x, int y, this.map, double fuel) {
    this.symbol = symbol;
    this.x = x;
    this.y = y;
    this.map = map;
    this.fuel = fuel;
  }

  bool checkBomb() {
    if (map!.isBomb(x, y)) {}
    return false;
  }

  bool canMove(int x, int y) {
    return fuel > 0 && map!.inMap(x, y) && canMove1(x, y);
  }
  bool canMove1(int x,int y){
    if(map!.isObstruction(x, y)==true){
      if(itemAttatk > 0){
        print(">>> break the barrier <<<");
        reduceAttatk();
      }else{
        return false;
      }
    }
    return true;
  }

  void checkFuel() {
    var fuel = map!.fillFuel(x, y);
    if (fuel > 0) {
      this.fuel += fuel;
    }
  }

  void checkster() {
    int star = map!.addStar(x, y);
    if (star >= 0) {
      this.star += star;
    }
  }

  void checkitemprotection() {
    int itemprotection = map!.addItem(x, y);
    if (itemprotection >= 0) {
      this.itemprotection += itemprotection;
    }
  }

  void checkitemAttatk(){
    int itemattatk = map!.addItem2(x, y);
    if(itemattatk > 0 ){
      this.itemAttatk += itemattatk;
    }
  }
  void reduceAttatk(){
    itemAttatk--;
  }


  void reduceitemprotection() {
    itemprotection--;
  }

  void reduceFuel() {
    fuel--;
  }

  void reduceFuel1() {
    fuel = fuel - 0.5 ;
  }

   @override
  bool move1(String direction) {
    switch (direction) {
      case "w":
        if (walkN()) return false;
        break;
      case "s":
        if (walkS()) return false;
        break;
      case "d":
        if (walkE()) return false;
        break;
      case "a":
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    checkBomb();
    return true;
  }

  bool walkN() {
    checkFuel();
    if (canMove(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    reduceFuel();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkS() {
    checkFuel();
    if (canMove(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    reduceFuel();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkE() {
    checkFuel();
    if (canMove(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    reduceFuel();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  bool walkW() {
    checkFuel();
    if (canMove(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    reduceFuel();
    checkitemprotection();
    checkitemAttatk();
    checkster();
    return false;
  }

  void setMap(Tablemap table) {
    this.map = table;
  }

  String printStatus() {
    return "==================================\n|| >>>" + " x = " + x.toString() + " , y = " + y.toString() +
        "\n|| >>> Fuel🛢️ : " + fuel.toString() +
        "\n|| >>> Star⭐ : " + star.toString() +
        "\n|| >>> Item Protect🛡️ : " + itemprotection.toString() +
        "\n|| >>> Item Attatk:⚔️ : "+ itemAttatk.toString() +
        "\n==================================";

  }
  


  
}
